import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCarComponent } from './add-car/add-car.component';
import { HomeComponent } from './home/home.component';
import { NewhomeComponent } from './add-car/newhome/newhome.component';
import { SingleDataComponent } from './single-data/single-data.component';

const routes: Routes = [
    {path:'home',component:NewhomeComponent},
    { path: 'addcar', component: AddCarComponent },
    {path:'single',component:SingleDataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
