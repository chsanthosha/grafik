import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { forkJoin, Observable, throwError } from 'rxjs';
import { Constant } from '../add-car/constant';
import { retry, } from 'rxjs/operators';
import { catchError, } from 'rxjs/operators';
import {

  HttpEvent,

  HttpInterceptor,

  HttpHandler,

  HttpRequest,

  HttpResponse,

  HttpErrorResponse

} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CarServicesService {

  constructor(private http: HttpClient) { }
  AddModel(obj: object): Observable<any> {
    return this.http.post(Constant.API_URL + 'addmodel/', obj, {})
      .pipe(map((res: any) => res)
      )
  }
  AddLine(obj: object): Observable<any> {
    return this.http.post(Constant.API_URL + 'addline/', obj, {})
      .pipe(map((res: any) => res)
      )
  }
}
