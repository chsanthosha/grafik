import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { CarServicesService} from '../add-car/car-services.service'
@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.scss']
})
export class AddCarComponent implements OnInit {

  ModelForm: FormGroup;
  LineForm: FormGroup;
  CarModels:any;
  CarModelName: any;

  constructor(private fb: FormBuilder,private car:CarServicesService) {}

  ngOnInit() {
    this.ModelForm = this.fb.group({
      ModelName: ['', Validators.required],
    });
    this.LineForm = this.fb.group({
      id:[''],
      CarLineModel:[''],
      CarLineName: ['', Validators.required],
    });
    // this.secondFormGroup = this._formBuilder.group({
    //   secondCtrl: ['', Validators.required],
    // });
  }
  onModel() {
    console.log(this.ModelForm.value)

    this.car.AddModel(this.ModelForm.value).subscribe((data) => {

      if (data) {
       console.log(data)
       this.CarModels=data
       this.CarModelName=data.ModelName
       

      }
     
    });
  }
  onLine(){
    this.LineForm.value.id=this.CarModels.id
    this.LineForm.value.CarLineModel=this.CarModels.ModelName
    console.log(this.LineForm.value)
    let obj={ModelName:this.CarModels.ModelName,
    id:this.CarModels.id,
    CarLines:[{CarLineName:this.LineForm.value.CarLineName}]
  }
    this.car.AddLine(obj).subscribe((data) => {

      if (data) {
       console.log(data)

      }
     
    });
  }
  }

