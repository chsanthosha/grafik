import { Component, OnInit } from '@angular/core';
import {ThemePalette} from '@angular/material/core';

export interface Task {
  name: string;
  completed: boolean;
  color: ThemePalette;
  subtasks?: Task[];
}
@Component({
  selector: 'app-single-data',
  templateUrl: './single-data.component.html',
  styleUrls: ['./single-data.component.scss']
})

export class SingleDataComponent implements OnInit {
  task: Task = {
    name: 'Rohbau/Presswerk',
    completed: false,
    color: 'primary',
    subtasks: [
      {name: 'Unterbau', completed: false, color: 'primary'},
      {name: 'Rückwand', completed: false, color: 'accent'},
      {name: 'Seitenwand links und rechts', completed: false, color: 'warn'},
      {name: 'Dach', completed: false, color: 'accent'},
    ],
  };

  allComplete: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }
  
  updateAllComplete() {
    this.allComplete = this.task.subtasks != null && this.task.subtasks.every(t => t.completed);
  }

  someComplete(): boolean {
    if (this.task.subtasks == null) {
      return false;
    }
    return this.task.subtasks.filter(t => t.completed).length > 0 && !this.allComplete;
  }

  setAll(completed: boolean) {
    this.allComplete = completed;
    if (this.task.subtasks == null) {
      return;
    }
    this.task.subtasks.forEach(t => (t.completed = completed));
  }
  DownloadPdf(){
    let link = document.createElement("a");
        link.download = "206_R_01_00W_004_000";
        link.href = "assets/Files/206_R_01_00W_004_000.pdf";
        link.click();

  }
  DownloadZip(){
    let link = document.createElement("a");
    link.download = "06_User Interface";
    link.href = "assets/Files/06_User Interface.zip";
    link.click();
  }
  DownloadExcel(){
    let link = document.createElement("a");
    link.download = "CHC_Pages";
    link.href = "assets/Files/CHC_Pages.xlsx";
    link.click();
  }

}
